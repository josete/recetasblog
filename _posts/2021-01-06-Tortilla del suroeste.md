--- 
layout: post 
title: "Tortilla del suroeste" 
date: 2021-01-06
categories: comida
ingredientes: 
- 1 tbsp de aceite de oliva virgen extra
- 1/4  de cebolla roja en dados
- 1/2  diente de ajo picado
- 1 tbsp de pimiento rojo o verde picado (o una mezcla)
- 2  lonchas finas de jamón, cortado en cubitos
- 1 tbsp de tomate fresco picado
- 2  huevos
-   Sal
pasos: 
- En una sartén grande a fuego medio, calentar el aceite de oliva
- Agregar el pimiento, el jamón y el tomate, y cocinar durante 2 o 3 minutos, hasta que el jamón comience a dorarse
- En un tazón pequeño, batir los huevos hasta que se combinen
- Agregar la mezcla de verduras y jamón al centro de la tortilla, extendiéndolo en una franja ancha por el centro
---